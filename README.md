# Cookiecutter PyPackage

Cookiecutter template for ESRF beamline python package.

## Quickstart

Install the latest Cookiecutter if you haven't installed it yet (this requires
Cookiecutter 1.4.0 or higher)::

```pip install -U cookiecutter```

Generate a Python package project::

```cookiecutter https://gitlab.esrf.fr/bliss/cookiecutter.git```

Then:

* Create a repo and put it there.
* Install the dev requirements into a virtualenv. (``pip install -r requirements_dev.txt``)
  and activate automated deployment on PyPI when you push a new tag to master branch.
* Release your package by pushing a new tag to master.
* Add a `requirements.txt` file that specifies the packages you will need for
  your project and their versions. For more info see the `pip docs for requirements files`.
