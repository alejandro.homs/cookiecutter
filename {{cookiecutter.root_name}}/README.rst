{% for _ in cookiecutter.project_name %}={% endfor %}
{{ cookiecutter.project_name }}
{% for _ in cookiecutter.project_name %}={% endfor %}

[![build status](https://gitlab.esrf.fr/{{ cookiecutter.beamline }}/{{cookiecutter.project_slug}}/badges/master/build.svg)](http://{{ cookiecutter.beamline }}.gitlab-pages.esrf.fr/{{ cookiecutter.beamline }})
[![coverage report](https://gitlab.esrf.fr/{{ cookiecutter.beamline }}/{{cookiecutter.project_slug}}/badges/master/coverage.svg)](http://{{ cookiecutter.beamline }}.gitlab-pages.esrf.fr/{{cookiecutter.project_slug}}/htmlcov)

{{ cookiecutter.project_short_description }}

Latest documentation from master can be found [here](http://{{ cookiecutter.beamline }}.gitlab-pages.esrf.fr/{{cookiecutter.project_slug}})
